# Overview
This is a sample template for a microservice structure

## Pre-requisites
- java 8
- gradle
 - docker

# Build
 - "distributionAll" task
 
    ` ./gradlew distributionAll `
    
# Deployment
## Docker
 - execute docker-compose up -d under build or preferred directory
 
## Local
 
### service-spring-rest
 - bootRun
 './gradlew :service-spring-rest:bootRun'
 
 
 TODO:
 
 - implement API Gateway
 - OAuth for security
 - Consul - for service discovery and health check
 - Kubernetes - to manage the containers /clusters / scaling
 - load balancer implementation thru docker
 - UI client