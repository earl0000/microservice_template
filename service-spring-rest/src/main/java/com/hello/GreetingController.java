package com.hello;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jagbay
 *
 * This is a demo class
 */
@RestController
public class GreetingController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private static final Logger logger = LogManager.getLogger(GreetingController.class.getName());

    @Value("${name}")
    private String nameConfig;

    /**
     * This is a Demo method
     * @return Greeting object
     */
    @RequestMapping("/greeting")
    public Greeting greeting() {
        logger.debug("Debugging log");
        logger.info("Info log");
        logger.warn("Hey, This is a warning!");
        logger.error("Oops! We have an Error. OK");
        logger.fatal("Damn! Fatal error. Please fix me.");
        return new Greeting(counter.incrementAndGet(),
                String.format(template, nameConfig));
    }

    /**
     * this is a demo method
     * @param name name to greet
     * @return String
     */
    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", defaultValue="World") String name){
        return new StringBuilder().append("Hello "+name+"!").toString();
    }
}
